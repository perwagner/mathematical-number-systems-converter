/*****************************************************************
Mathematical number systems converter

Converts numbers between different mathematical numbersystems.
To/from hex
To/from binary


Written by: Per Wagner Nielsen

*****************************************************************/
/* Include pre-processor directives */
#include <stdio.h>
#define BYTETOBINARYPATTERN "%d%d%d%d%d%d%d%d"
#define BYTETOBINARY(byte)  \
  (byte & 0x80 ? 1 : 0), \
  (byte & 0x40 ? 1 : 0), \
  (byte & 0x20 ? 1 : 0), \
  (byte & 0x10 ? 1 : 0), \
  (byte & 0x08 ? 1 : 0), \
  (byte & 0x04 ? 1 : 0), \
  (byte & 0x02 ? 1 : 0), \
  (byte & 0x01 ? 1 : 0)

void convertToBinary();

int main()
{
    int a;
    char selection;

    /* Menu for selection conversion */
    while(selection!='q')
    {
        printf("***** Mathematical number systems converter *****");
        printf("\n(b) Convert to binary format");

        printf("\n(q) leave program");
        printf("\n");
        scanf("%c",&selection);

        /* Choose the function to do the conversion */
        switch (selection)
        {
            case 'b':
            {
                convertToBinary();
                fflush(stdin);
                break;
            }
        }
    }

    return 0;
}

void convertToBinary()
{
    int number;

    /* Enter value to be converted */
    printf("\n\nEnter number you want to convert in the 10 number system: ");
    scanf("%d",&number);
    /* show in binary */
    printf("\n%d in binary format: %d%d%d%d%d%d%d%d\n\n",number,BYTETOBINARY(number));
}
